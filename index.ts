const argv = require('argv') // eslint-disable-line @typescript-eslint/no-var-requires
const args = argv.option({
    name: 'path-env',
    short: 'e',
    type: 'string',
    description: 'where is the .env located',
    example: '--path-env=_dev.env'
}).run()
const envPath = args.options['path-env'] || '.env'

// Notify for any unexpected crashes
process.on('uncaughtException', (err) => {
    console.error('uncaughtException', err.name, err.message)
    console.error(err.stack)

    process.exit(1)
})

console.log(`running on process ${process.pid}`)
console.log('env path', envPath)
require('dotenv').config({ path: envPath }) // eslint-disable-line @typescript-eslint/no-var-requires

import { AppServer } from './src/app'
import assert from 'assert'
import fs from 'fs'
import path from 'path'
import sqlite3 from 'better-sqlite3'
import { TwitterPoller } from './src/poller'

async function main() {
    // open the db
    const dbPath = process.env.DB_PATH || './db/indexer.db'
    console.log('database path', dbPath)
    if (!fs.existsSync(dbPath)) {
        console.log('no existing database found, initializing a new db')
        const pathBase = path.basename(dbPath)
        const pathDir = path.dirname(dbPath)
        const pathExt = path.extname(dbPath)
        assert(pathExt == '.db', 'please only .db sqlite databases')
        if (!fs.existsSync(pathDir)) {
            fs.mkdirSync(pathDir)
        }



        // clear out and init the DB
        const db: sqlite3.Database = new sqlite3(dbPath, { verbose: console.log, fileMustExist: false })

        // https://sqlite.org/foreignkeys.html
        db.pragma('foreign_keys=ON')
        db.exec(fs.readFileSync('./scripts/db_schema.sql', 'utf8'))
        db.close()
    }

    const db: sqlite3.Database = new sqlite3(dbPath, { fileMustExist: true })
    const twitterPoller = new TwitterPoller(db)
    const app = new AppServer(__dirname, db)
}
(() => {
    main()
})()