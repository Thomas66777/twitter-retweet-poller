BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "retweets" (
	"id"	INTEGER,
	"twitter_id"	TEXT NOT NULL,
	"type"	TEXT NOT NULL,
	"time_stamp"	timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	UNIQUE("twitter_id","type"),
	PRIMARY KEY("id" AUTOINCREMENT)
);

CREATE TABLE IF NOT EXISTS "followers" (
	"id"	INTEGER,
	"twitter_id"	TEXT NOT NULL UNIQUE,
	"time_stamp"	timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY("id" AUTOINCREMENT)
);

CREATE TABLE IF NOT EXISTS "twitter_to_pkh" (
	"twitter_id"	TEXT,
	"pkh"	TEXT NOT NULL,
	PRIMARY KEY("twitter_id")
);
COMMIT;
