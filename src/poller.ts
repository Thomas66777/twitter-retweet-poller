import { sleep } from './utils-functions'
import Twitter from 'twitter'
import { followers, retweetIds, retweetQuotes } from './twitter'
import sqlite3 from 'better-sqlite3'

export class TwitterPoller {
    tweet_id: string;
    clientTwitter: any;
    db: sqlite3.Database;
    counter: number;
    constructor(db: sqlite3.Database) {
        this.counter = 0
        this.tweet_id = process.env.TWEET_ID
        this.db = db
        this.clientTwitter = new Twitter({
            consumer_key: process.env.CONSUMER_KEY,
            consumer_secret: process.env.CONSUMER_SECRET,
            access_token_key: process.env.ACCESS_TOKEN_KEY,
            access_token_secret: process.env.ACCESS_TOKEN_SECRET,
        })
        this.startPolling()
    }
    async startPolling() {
        try {
            // Get all the retweets
            const aryRetweetIds = await retweetIds(this.clientTwitter, this.tweet_id)

            for (const twitter_id of aryRetweetIds) {
                const res = this.db.prepare('insert or ignore into retweets (twitter_id, type) values (:twitter_id, :type)').run({ twitter_id, type: 'retweet' })
                if (res.changes) {
                    console.info(`${new Date().toISOString()} retweet ${twitter_id}`)
                }
            }


            const aryRetweetQuotes = await retweetQuotes(this.clientTwitter, this.tweet_id)
            for (const twitter_id of aryRetweetQuotes) {
                const res = this.db.prepare('insert or ignore into retweets (twitter_id, type) values (:twitter_id, :type)').run({ twitter_id, type: 'quote' })
                if (res.changes) {
                    console.info(`${new Date().toISOString()} quote ${twitter_id}`)
                }
            }

            // every 10 min
            if (this.counter % 10 === 0) {
                const aryFollowers = await followers(this.clientTwitter, process.env.THEALCHEMEMIST)
                for (const twitter_id of aryFollowers) {
                    const res = this.db.prepare('insert or ignore into followers (twitter_id) values (:twitter_id)').run({ twitter_id })
                    if (res.changes) {
                        console.info(`${new Date().toISOString()} follower ${twitter_id}`)
                    }
                }
            }

        } catch (error) {
            console.error(error.message)
        } finally {
            this.counter++
            await sleep(Number(process.env.POLLING_FREQUENCY))
            await this.startPolling()
        }
    }
}