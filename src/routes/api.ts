import { Router, Request, Response, request } from 'express'
import jsend from 'jsend'
import assert from 'assert'
import { ContractAbstraction, ContractProvider, TezosToolkit } from '@taquito/taquito'
import { Database } from 'better-sqlite3'
import { validContractAddress } from '../utils-tezos-keys'
import { validSignedMessage } from '../utils-crypto'
import { getPubkeyHash } from '../utils-sig'

const router: Router = Router()

router.get('/', async (req: Request, res: Response) => {
    res.status(200).json(jsend.success(true))
})

router.get('/status/:address?', async (req: Request, res: Response) => {
    const pkh = req.query.address || req.params.address
    assert(pkh, 'must provide a Tezos Address')

    const db: Database = req.app.get('db')
    const isExists = db.prepare('select count(*) from claim where pkh = :pkh').pluck().get({ pkh })

    res.status(200).json(jsend.success(isExists))
})

router.post('/claim', async (req: Request, res: Response) => {
    try {
        const signature = req.body.signature
        const message = req.body.message
        const publicKey = req.body.publicKey

        assert(signature && message && publicKey, 'missing required paramaters')

        const db: Database = req.app.get('db')
        assert(message === process.env.MESSAGE_SIGNED)

        const { pkh } = await validSignedMessage(signature, publicKey, message)

        const isExists = db.prepare(`
        insert or ignore into claim (pkh, pk, ip, signature, message)
        values (:pkh, :pk, :ip, :signature, :message)
        `).run({})




        // res.status(200).json(jsend.success({
        //     quest_id,
        //     tezos_account,
        //     game_id,
        // }))
    } catch (error) {
        res.status(400).json(jsend.error(error))

    }
})
export const ApiController: Router = router
