{
    "openapi": "3.0.0",
    "info": {
        "version": "1.0.0",
        "title": "Tezos Quest Indexer",
        "description": "API endpoints for Quest Indexer"
    },
    "tags": [
        {
            "name": "Quest",
            "description": "API for Quest Indexer"
        }
    ],
    "consumes": [
        "application/json"
    ],
    "produces": [
        "application/json"
    ],
    "paths": {
        "/": {
            "get": {
                "summary": "check if the API is running, always returns true",
                "produces": [
                    "application/json"
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "content": {
                            "application/json": {
                                "examples": {
                                    "---": {
                                        "status": "success",
                                        "data": true
                                    }
                                }
                            }
                        }
                    }
                }
            }
        },
        "/status": {
            "get": {
                "summary": "gets the current status of the poller",
                "produces": [
                    "application/json"
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "content": {
                            "application/json": {
                                "examples": {
                                    "---": {
                                        "status": "success",
                                        "data": {
                                            "chain_id": "NetXm8tYqnMWky1",
                                            "block_timestamp": "2021-02-05T23:48:37.000Z",
                                            "block_level": 423886,
                                            "block_id": "BLWR9BzVj1HX8sokYPVNdnrxZaeVZNsbcehNFhVKpsrfqahTwce"
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        },
        "/game_id": {
            "get": {
                "summary": "gets the game_id for the specific tezos address and quest puzzle",
                "produces": [
                    "application/json"
                ],
                "parameters": [
                    {
                        "name": "tezos_account",
                        "in": "query",
                        "description": "The tezos address (tz1...) of the user",
                        "required": true,
                        "schema": {
                            "type": "string"
                        }
                    },
                    {
                        "name": "quest_id",
                        "in": "query",
                        "description": "The Quest Hash for the specific game; if not provided defaults to this poller",
                        "required": false,
                        "schema": {
                            "type": "string"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "content": {
                            "application/json": {
                                "examples": {
                                    "tz1NXuEyKGjGXAEn5gSqzgwhkEqNeN5wcaLf": {
                                        "status": "success",
                                        "data": {
                                            "quest_id": "c89b8a9734688932a5dc6e68acb18f0ccbd98bebe4e5343dce66023f41637078",
                                            "tezos_account": "tz1NXuEyKGjGXAEn5gSqzgwhkEqNeN5wcaLf",
                                            "game_id": "2244295628"
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        },
        "/rewards_pending/{quest_id}": {
            "get": {
                "summary": "gets the all the pending FA2 transfers needed to be sent by an admin to the FA2 contract",
                "produces": [
                    "application/json"
                ],
                "parameters": [
                    {
                        "name": "quest_id",
                        "in": "query",
                        "description": "the quest_id to check for any pending reward",
                        "required": true,
                        "schema": {
                            "type": "number"
                        }
                    },
                    {
                        "name": "from",
                        "in": "query",
                        "description": "this overrides the tezos FA2 contract account you would to send FA2 from",
                        "required": false,
                        "schema": {
                            "type": "string"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "content": {
                            "application/json": {
                                "examples": {
                                    "---": {
                                        "status": "success",
                                        "data": {
                                            "humanArgs": [
                                                [
                                                    {
                                                        "from_": "tz1NXuEyKGjGXAEn5gSqzgwhkEqNeN5wcaLf",
                                                        "txs": [
                                                            {
                                                                "to_": "tz1Myo4eJ6JaGEPkTJw3cf1R3MQEmZQmkGt4",
                                                                "token_id": 20,
                                                                "amount": 0
                                                            },
                                                            {
                                                                "to_": "tz1PirboHQVqkYqLSWfHUHEy3AdhYUNJpvGy",
                                                                "token_id": 13,
                                                                "amount": 0
                                                            }
                                                        ]
                                                    }
                                                ]
                                            ],
                                            "transferParamsFA2": [
                                                {
                                                    "to": "KT1PS2jZVzNMW54UsnqBqwwkArXnAZ29jiTF",
                                                    "amount": 0,
                                                    "mutez": false,
                                                    "parameter": {
                                                        "entrypoint": "transfer",
                                                        "value": [
                                                            {
                                                                "prim": "Pair",
                                                                "args": [
                                                                    {
                                                                        "string": "tz1NXuEyKGjGXAEn5gSqzgwhkEqNeN5wcaLf"
                                                                    },
                                                                    [
                                                                        {
                                                                            "prim": "Pair",
                                                                            "args": [
                                                                                {
                                                                                    "string": "tz1Myo4eJ6JaGEPkTJw3cf1R3MQEmZQmkGt4"
                                                                                },
                                                                                {
                                                                                    "prim": "Pair",
                                                                                    "args": [
                                                                                        {
                                                                                            "int": "20"
                                                                                        },
                                                                                        {
                                                                                            "int": "0"
                                                                                        }
                                                                                    ]
                                                                                }
                                                                            ]
                                                                        },
                                                                        {
                                                                            "prim": "Pair",
                                                                            "args": [
                                                                                {
                                                                                    "string": "tz1PirboHQVqkYqLSWfHUHEy3AdhYUNJpvGy"
                                                                                },
                                                                                {
                                                                                    "prim": "Pair",
                                                                                    "args": [
                                                                                        {
                                                                                            "int": "13"
                                                                                        },
                                                                                        {
                                                                                            "int": "0"
                                                                                        }
                                                                                    ]
                                                                                }
                                                                            ]
                                                                        }
                                                                    ]
                                                                ]
                                                            }
                                                        ]
                                                    }
                                                }
                                            ]
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        },
        "/history": {
            "get": {
                "summary": "gets all the data associated with a specifc account and Quest game_id",
                "produces": [
                    "application/json"
                ],
                "parameters": [
                    {
                        "name": "quest_id",
                        "in": "query",
                        "description": "the quest_id of the user you are looking for",
                        "required": true,
                        "schema": {
                            "type": "string"
                        }
                    },
                    {
                        "name": "game_id",
                        "in": "query",
                        "description": "the game_id of the user you are looking for",
                        "required": false,
                        "schema": {
                            "type": "string"
                        }
                    },
                    {
                        "name": "reward_status",
                        "in": "query",
                        "description": "the reward_status of the FA2: 1, 2, 3 | DETECTED_ON_CHAIN, AWAITING_ADMIN_TRANSFER, COMPLETE",
                        "required": false,
                        "schema": {
                            "type": "string"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "content": {
                            "application/json": {
                                "examples": {
                                    "2244295628": {
                                        "status": "success",
                                        "data": [
                                            {
                                                "id": 1,
                                                "quest_id": 1,
                                                "game_id": 2832819860,
                                                "token_id": 6,
                                                "reward_status": 2,
                                                "reward_account": "tz1NXuEyKGjGXAEn5gSqzgwhkEqNeN5wcaLf",
                                                "filter_id": 4,
                                                "time_stamp": "2021-02-09T22:47:40Z",
                                                "block_level": 435077,
                                                "operation_idx": 3,
                                                "chain_id": "NetXm8tYqnMWky1",
                                                "hash": "oouVQmLVVCwFKK9CrcBGaV6gToZdwjuiBgv7mHyqCU5PavdePje",
                                                "reward_hash": null,
                                                "reward_block_level": null
                                            }
                                        ]
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}