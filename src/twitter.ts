// import TorusUtils from '@toruslabs/torus.js'
// import FetchNodeDetails from '@toruslabs/fetch-node-details'
import Twitter from 'twitter'
global.fetch = require('node-fetch')
import * as elliptic from 'elliptic'
import { b58cdecode, b58cencode, prefix } from '@taquito/utils'
import { blake2b } from 'blakejs'
import assert from 'assert'

// const verifierTwitter = {
//     name: 'Twitter',
//     typeOfLogin: 'twitter',
//     clientId: 'vKFgnaYZzKLUnhxnX5xqTqeMcumdVTz1',
//     verifier: 'tezos-twitter-test',
//     caseSensitiveVerifierID: false
// }
const verifierTwitter = {
    name: 'Twitter',
    typeOfLogin: 'twitter',
    clientId: 'UJl5d4iHVgbrAaSlucXNf2F2uKlC0m25',
    verifier: 'tezos-twitter',
    caseSensitiveVerifierID: false
}
async function twitterLookup(username?: string, id?: string) {
    let req = {}
    if ((id && username) || (!id && !username)) {
        console.log({ username, id })
        throw new Error('Invalid input')
    } else if (id) {
        req = { id }
    } else {
        req = { username: username.replace('@', '') }
    }
    return await fetch('https://api.tezos.help/twitter-lookup/', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        referrerPolicy: 'no-referrer',
        body: JSON.stringify(req)
    }).then(
        (ans) => {
            return ans.json()
        }
    )
}

// export async function lookupTwitter(verifierId: string) {
//     const verifier = verifierTwitter.verifier

//     let sanitizedVerifierId = verifierId
//     if (!verifierTwitter.caseSensitiveVerifierID) {
//         sanitizedVerifierId = sanitizedVerifierId.toLocaleLowerCase()
//     }

//     let twitterId = ''
//     const username = sanitizedVerifierId.replace('@', '')
//     const { id } = await twitterLookup(username)

//     const twitterIdValid = (id: string) => {
//         const re = /^[0-9]+$/
//         return re.test(id)
//     }

//     if (twitterIdValid(id)) {
//         sanitizedVerifierId = `twitter|${id}`
//         twitterId = id
//     } else {
//         throw new Error('Twitter handle not found')
//     }
//     // const proxy = { address: '0x4023d2a0D330bF11426B12C6144Cfb96B7fa6183', network: 'ropsten' }
//     const proxy = { address: '0x638646503746d5456209e33a2ff5e3226d698bea', network: 'mainnet' }
//     const fetchNodeDetails = new FetchNodeDetails({ network: proxy.network, proxyAddress: proxy.address })
//     const { torusNodeEndpoints, torusNodePub, torusIndexes } = await fetchNodeDetails.getNodeDetails()
//     const torus = new TorusUtils()
//     const pk: any = await torus.getPublicAddress(torusNodeEndpoints, torusNodePub, { verifier, verifierId: sanitizedVerifierId }, true)

//     // const pk = { "address": "0x4590516867c1B92AAe633f8103e108E8E40C231b", "X": "80daed4dead840042acf559a1552fa96d904788f9d81baa0e3d281f2cf8a808c", "Y": "f66f216e344a3fc06f850d6b396e5a2d90817f5910cb939b5a997d6aadb03716" }
//     const pkh = spPointsToPkh(pk.X, pk.Y)
//     console.log(pk, pkh)
//     assert('tz2VjN4Z6jwBo1NCwyq63swLfqCcqUswoCjs' === pkh)
//     return { pkh, twitterId }

// }

function spPointsToPkh(pubX: string, pubY: string): string {
    const key = (new elliptic.ec('secp256k1')).keyFromPublic({ x: pubX, y: pubY })
    const prefixVal = key.getPublic().getY().toArray()[31] % 2 ? 3 : 2
    const pad = new Array(32).fill(0)
    const publicKey = new Uint8Array(
        [prefixVal].concat(pad.concat(key.getPublic().getX().toArray()).slice(-32)
        ))
    const pk = b58cencode(publicKey, prefix.sppk)
    // sppk7aHPHfix9pdd8TAR9NwhNo83wFpAoSoWaAfHJvyRgRY5UGfEXoU
    const pkh = pk2pkh(pk)
    // tz2Rx3Prxq5Ka5MQqLSmheKbeqYZeMGqFfJ4
    return pkh
}
function pk2pkh(pk: string): string {
    if (pk.length === 54 && pk.slice(0, 4) === 'edpk') {
        const pkDecoded = b58cdecode(pk, prefix.edpk)
        return b58cencode(blake2b(pkDecoded, null, 20), prefix.tz1)
    } else if (pk.length === 55 && pk.slice(0, 4) === 'sppk') {
        const pkDecoded = b58cdecode(pk, prefix.edpk)
        return b58cencode(blake2b(pkDecoded, null, 20), prefix.tz2)
    }
    throw new Error('Invalid public key')
}

export async function retweetIds(client, tweet_id: string): Promise<string[]> {
    const res = await twitterAsPromise(client, 'statuses/retweeters/ids', { id: tweet_id, stringify_ids: true })
    const tweets = res.tweets
    return tweets.ids
}

export async function retweetQuotes(client, tweet_id: string): Promise<string[]> {
    const res = await twitterAsPromise(client, 'search/tweets', { q: tweet_id, })
    const aryQuotes = res.tweets.statuses.filter(t => t.quoted_status_id_str == tweet_id).map(u => u.user.id_str)
    // console.log(res.tweets.search_metadata.next_results)

    return aryQuotes
}

export async function followers(client, twitter_id: string): Promise<string[]> {
    const res = await twitterAsPromise(client, 'followers/ids', { id: twitter_id, stringify_ids: true })
    const tweets = res.tweets
    return tweets.ids
}


export async function twitterAPI() {
    // * like; 
    // * retweet; 
    // * follow; 
    // * retweet with tag of three friends
    // https://developer.twitter.com/en/docs/api-reference-index

    const client = new Twitter({
        consumer_key: 'e8d9CD9FGrZSME1fHFpX88rIQ',
        consumer_secret: 'UTjS52owTxW0nQShvu5b91mA9uC4Pa0S9REpwHH8fcUvz1MBGP',
        access_token_key: '1000319947724255232-5fDjMnXITatV1wchbbSFB0OPTGY9f1',
        access_token_secret: 'qFTezQkiEunyt5MzS0tq9OVR5JnpoeSaFVA0EvD01h51A',
    })

    // https://developer.twitter.com/en/docs/twitter-api/v1/tweets/post-and-engage/api-reference/get-statuses-show-id
    // const res = await twitterAsPromise(client, 'search/tweets', { q: '@ElonTrades' })
    // const res = await twitterAsPromise(client, 'statuses/show', { id: '1365072746649108480' })

    // Your credentials do not allow access to this resource
    // const res = await twitterAsPromise(client, 'statuses/retweets', { id: '1365072746649108480', trim_user: true })
    const res = await twitterAsPromise(client, 'statuses/retweeters/ids', { id: '1365072746649108480', trim_user: true })
    const tweets = res.tweets

    // const tweets = retweets
    const aryUserIds = []
    for (const tweet of tweets) {
        aryUserIds.push(tweet.user.id_str)
    }
    // https://developer.twitter.com/en/docs/twitter-api/v1/accounts-and-users/follow-search-get-users/api-reference/get-users-lookup
    // const res = await twitterAsPromise(client, 'users/lookup', { user_id: aryUserIds.join(','), trim_user: true })


    console.log()
}

async function twitterAsPromise(client, path: string, params = {}): Promise<{ tweets: any, response: any }> {
    return new Promise((resolve, reject) => {
        client.get(path, params, (error, tweets, response) => {
            if (error) {
                const errMsg = Array.isArray(error) ? error[0].message : error.message
                reject(new Error(errMsg))
                return
            }
            resolve({ tweets, response })
        })

    })
}