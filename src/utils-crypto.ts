import crypto from 'crypto' // This is node.js library https://nodejs.org/api/crypto.html
import assert from 'assert'
import { Big } from 'big.js'
import { validateSignature, ValidationResult } from '@taquito/utils'
import { validAddress, validImplicitAddress, validSecretKey } from './utils-tezos-keys'
import { getPubkeyHash, verifySig } from './utils-sig'

const pad = (num, paddingLen = 8) => {
    return num.toString(16).padStart(paddingLen, '0')
}

const stringEncoder = value => {
    const str = Buffer.from(value, 'utf8').toString('hex')
    const hexLength = str.length / 2
    return `01${pad(hexLength)}${str}`
}

export async function validSignedMessage(signature: string, publicKey: string, msgSigned: string) {
    assert(validateSignature(signature) === ValidationResult.VALID, 'invalid signature')
    const pkh = await getPubkeyHash(publicKey)
    assert(validImplicitAddress(pkh) === true, 'invlaid public key')
    const msgEncoded = stringEncoder(msgSigned)

    const isValid = await verifySig(msgEncoded, signature, publicKey)
    assert(isValid === true, 'signature is invalid')
    return {
        msgEncoded, pkh
    }
}
