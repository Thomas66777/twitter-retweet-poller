import { Big } from 'big.js'
import assert from 'assert'
import { Database } from 'better-sqlite3'

export interface ITransactionBatch { sql: string, params?: Object }
export function dbTransactionBatch(db: Database, batch: ITransactionBatch[]): void {
    const res = db.transaction((args) => {
        for (const { sql, params } of args) {
            db.prepare(sql).run(params || {})
        }
    })
    res(batch)
}

export function numToBuffer(num: string | number, bits: number): Uint8Array {
    Big.RM = 0
    let res = new Big(num)
    // Max size that can fit is pow(2,bytes) - 1
    assert(res.lt(new Big(2).pow(bits)), 'number too big')

    let leftShift = bits - 8
    assert(bits % 8 === 0, 'invalid byte number')
    const aryBuf: Uint8Array = new Uint8Array(bits / 8)
    while (leftShift >= 0) {
        const shiftBits = new Big(2).pow(leftShift)
        const pos = Number(res.div(shiftBits).toFixed(0))
        aryBuf[leftShift / 8] = pos

        res = res.minus(shiftBits.times(pos))
        leftShift -= 8
    }


    return aryBuf
}