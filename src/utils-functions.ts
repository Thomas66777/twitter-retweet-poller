import { BlockResponse, OperationEntry, RpcClient } from '@taquito/rpc'

export function sleep(ms: number): Promise<void> {
    return new Promise(resolve => setTimeout(resolve, ms))
}
