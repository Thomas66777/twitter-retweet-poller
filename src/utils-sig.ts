import * as sotez from 'sotez'
import sodium from 'libsodium-wrappers'
import { blake2b } from 'blakejs'
import { ec as EC } from 'elliptic'

// eslint-disable-next-line @typescript-eslint/no-var-requires
// const EC = require('elliptic').ec

const isHex = function (s) {
    return (sotez.utility.buf2hex(sotez.utility.hex2buf(s)) === s.toLowerCase())
}

export const verifySig = async (bytes, sig, pk): Promise<boolean> => {
    await sodium.ready

    bytes = sotez.utility.hex2buf(bytes)
    const sigCurve = sig.substring(0, 2)
    switch (sigCurve) {
        case 'ed':
            sig = sotez.utility.b58cdecode(sig, sotez.prefix.edsig)
            break
        case 'sp':
            sig = sotez.utility.b58cdecode(sig, sotez.prefix.spsig)
            break
        case 'p2':
            sig = sotez.utility.b58cdecode(sig, sotez.prefix.p2sig)
            break
        default:
            sig = sotez.utility.b58cdecode(sig, sotez.prefix.sig)
            break
    }

    // pk is hex format (Beacon does this for reasons not clear)
    if (pk.length == 64 && isHex(pk)) {
        pk = Buffer.from(sotez.utility.hex2buf(pk))
        bytes = sodium.crypto_generichash(32, bytes)
        return sodium.crypto_sign_verify_detached(sig, bytes, pk)
    }

    const curve = pk.substring(0, 2)
    switch (curve) {
        case 'ed':
            bytes = sodium.crypto_generichash(32, bytes)
            pk = sotez.utility.b58cdecode(pk, sotez.prefix.edpk)
            return sodium.crypto_sign_verify_detached(sig, bytes, pk)
            break
        case 'sp':
        case 'p2':
            bytes = blake2b(32).update(bytes).digest()
            pk = sotez.utility.b58cdecode(pk, curve == 'sp' ? sotez.prefix.sppk : sotez.prefix.p2pk)
            // eslint-disable-next-line no-case-declarations
            const key = new EC(curve == 'sp' ? 'secp256k1' : 'p256').keyFromPublic(pk)
            return key.verify(bytes, { r: sig.slice(0, 32), s: sig.slice(32) })
            break
    }

    return false
}

export const getPubkeyHash = async (pk) => {
    await sodium.ready

    // pk is hex format (Beacon does this)
    if (pk.length == 64 && isHex(pk)) {
        pk = Buffer.from(sotez.utility.hex2buf(pk))
        return sotez.utility.b58cencode(sodium.crypto_generichash(20, pk), sotez.prefix.tz1)
    }

    const curve = pk.substring(0, 2)
    switch (curve) {
        case 'ed':
            pk = sotez.utility.b58cdecode(pk, sotez.prefix.edpk)
            return sotez.utility.b58cencode(sodium.crypto_generichash(20, pk), sotez.prefix.tz1)
            break
        case 'sp':
            pk = sotez.utility.b58cdecode(pk, sotez.prefix.sppk)
            return sotez.utility.b58cencode(sodium.crypto_generichash(20, pk), sotez.prefix.tz2)
            break
        case 'p2':
            pk = sotez.utility.b58cdecode(pk, sotez.prefix.p2pk)
            return sotez.utility.b58cencode(sodium.crypto_generichash(20, pk), sotez.prefix.tz3)
            break
    }
}

